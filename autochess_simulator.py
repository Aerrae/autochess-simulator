import random
import stats_mokkula as stats
import matplotlib.pyplot as plt
import result_transformer as rst


class Player:
    def __init__(self, id_number):
        self.id_number = id_number
        self.strength = random.random()
        self.gold = 0
        self.streak = 0
        self.losses = 0
        self.wins = 0

    def win(self):
        """Winning increases gold amount by length of winning streak 2 wins = 2 gold, maximum of 4"""
        self.wins += 1
        if self.streak < 1:
            # Streak variable is reset to 1 if last round was lost or it's the first round
            self.streak = 1
        else:
            self.streak += 1
            
        gold_amount = min(self.streak, 4)
        self.gold += gold_amount
            
    def lose(self):
        """Losing increased gold amount by length of losing streak 2 losses = 1 gold, maximum of 3"""
        self.losses += 1
        if self.streak >= 0:
            # Streak variable is set to 0 if last round was won or it's the first round
            self.streak = -1
        else:
            gold_amount = min(abs(self.streak), 3)
            self.gold += gold_amount
            self.streak -= 1 # streak increased after adjusting gold

    def __str__(self):
        return """Strength: {}, gold: {}, wins: {}, losses: {}""".format(self.strength, self.gold, self.wins, self.losses)


def play_round(players):
    """Shuffles a list of player objects and makes them fight one another."""
    random.shuffle(players)
    # currently works on only paired amount
    group1 = players[0:int(len(players)/2)] # convert back to int, because Python converts division to float by default
    group2 = players[int(len(players)/2):]
    for player1, player2 in zip(group1, group2):
        fight(player1, player2)


def fight(player1, player2):
    """Compare strength attribute of two objects and call their win and lose functions accordingly.
    Defending player loses if strengths are equal"""
    if player1.strength > player2.strength:
        player1.win()
        player2.lose()
    else:
        player1.lose()
        player2.win()


def play_game(players):
    """Plays a game of autochess i.e. simulates 40 rounds of 8 players playing"""
    n_rounds = 40 # number of rounds to be played
    for j in range(0, n_rounds):
        play_round(players) 

    sorted_players = sorted(players, key=lambda p: p.strength)
    return [{'strength' : s_player.strength, 'gold' : s_player.gold, 'wins' : s_player.wins, 'losses' : s_player.losses} for s_player in sorted_players]


def game_simulation(n_games):
    """Runs n_games amount of game simulations"""
    playerdata = {}
    for i in range(0, 8):
        playerdata[i] = []

    for g in range(n_games):
        players = [Player(p) for p in range(0, 8)]  # initial player list
        results = play_game(players)
        for j in range(0, 8):
            playerdata[j].append(results[j])
    return playerdata


def main():
    n_games = 1000
    playerdata = game_simulation(n_games)
    result_table = []
    for i in range(0, 8):
        goldlist = [pd["gold"] for pd in playerdata[i]]
        d_mean, d_std, d_vari = stats.run_basic_statistics(goldlist)
        std_bottom = d_mean - d_std
        std_top = d_mean + d_std
        datarow = [i, d_std, std_top, std_bottom, d_mean]
        result_table.append(datarow)
    print(result_table)
    header = ["Name", "Std", "One std max", "One std min", "Mean"] 

    create_results = input("Do you want to create result excel? ")
    if "y" in create_results.lower():
        print("Creating excel")
        rst.create_result_excel(header, result_table, "results.xlsx")


if __name__ == "__main__": 
    main()

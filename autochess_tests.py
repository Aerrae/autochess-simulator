import unittest
import autochess_simulator as ac_sim


class FightTestcase(unittest.TestCase):
    def setUp(self):
        # setup is ran again before every test case, so the players will be recreated
        self.players = [ac_sim.Player(i) for i in range(0,8)] 
        for player in self.players:
            # we want player strengths to stay constant for the sake of test cases
            player.strength = player.id_number

    def test_fight_equal_strength(self):
        player1 = self.players[0]
        player2 = self.players[1]
        #set the same strengths
        player2.strength = player1.strength

        ac_sim.fight(player1, player2)
        self.assertTrue(player1.losses == 1 and player2.wins == 1, 'Incorrect behavior, defending player should lose when evenly matched')



unittest.main()

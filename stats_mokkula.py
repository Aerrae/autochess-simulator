import math


def mean(data):
    """Takes one dimensional list as argument and returns the mean"""
    return sum(data)/len(data)


def variance(data):
    """Calculates variance from list of datapoints"""
    d_mean = mean(data)
    data_squared = [d**2 for d in data]
    d_squared_mean = mean(data_squared)
    return d_squared_mean - d_mean**2
    

def standard_deviation(data):
    """Calculates standard deviation from list of datapoints"""
    vari = variance(data)
    return math.sqrt(vari)


def run_basic_statistics(data):
    """Calculates standard deviation, variance and mean from list of datapoints"""
    data_squared = [d**2 for d in data]
    d_mean = mean(data)
    d_squared_mean = mean(data_squared)
    vari = d_squared_mean - d_mean**2
    std = math.sqrt(vari)
    return d_mean, std, vari


def main():
    data = [5,5,5,5,5,5,5,5,5,5,5,5,5,10,12]
    data_mean = mean(data)
    vari = variance(data)
    print(data_mean)
    print(vari)
    print(standard_deviation(data))
    print(str(optimized_calculations(data)))


if __name__=="__main__":
    main()
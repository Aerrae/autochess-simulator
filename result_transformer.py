import openpyxl


def create_result_excel(header, data, filename):
    wb = openpyxl.Workbook()
    xl_filename = filename
    ws = wb.active
    print(header)
    ws.append(header)
    for row in data:
        print(row)
        ws.append(row)

    letter_range = 'ABCDEFGHIJKLMNOPQRSTUVXYZ'
    rowlen = letter_range[len(data[0]) - 1]
    colheight = str(len(data) + 1)
    print(colheight)
    print(rowlen)
    cellrange = "{0}{1}:{2}{3}".format('A', '1', rowlen, colheight)
    tab = openpyxl.worksheet.table.Table(displayName="Table1", ref=cellrange)

    # Add a default style with striped rows and banded columns
    style = openpyxl.worksheet.table.TableStyleInfo(name="TableStyleMedium9", showFirstColumn=True,
                           showLastColumn=True, showRowStripes=True, showColumnStripes=True)
    tab.tableStyleInfo = style
    ws.add_table(tab)

    # High-low-close
    c1 = openpyxl.chart.StockChart()
    labels = openpyxl.chart.Reference(ws, min_col=1, min_row=2, max_row=9)
    data = openpyxl.chart.Reference(ws, min_col=3, max_col=5, min_row=1, max_row=9)
    c1.add_data(data, titles_from_data=True)
    c1.set_categories(labels)
    for s in c1.series:
        s.graphicalProperties.line.noFill = True
    # marker for close
    s.marker.symbol = "dot"
    s.marker.size = 5
    c1.title = "High-low-close"
    c1.hiLowLines = openpyxl.chart.axis.ChartLines()
    #c1.upDownBars = openpyxl.chart.updown_bars.UpDownBars()

    # Excel is broken and needs a cache of values in order to display hiLoLines :-/
    from openpyxl.chart.data_source import NumData, NumVal
    pts = [NumVal(idx=i) for i in range(len(data) - 1)]
    cache = NumData(pt=pts)
    c1.series[-1].val.numRef.numCache = cache

    ws.add_chart(c1, "A10")

    wb.save(xl_filename)
